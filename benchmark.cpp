//Project Radix_sort
#include <iostream>
#include <vector>
#include "sort.h"
#include <algorithm>
#include "cmath"
#include <ctime>

using Ttype = unsigned int;
using Tpair = std::pair <Ttype, Ttype>;

bool pairCompair(const Tpair &a, const Tpair &b);

double runStdSort(std::vector<Tpair> &data);

double runRadixSort(std::vector<unsigned int> &, std::vector<unsigned int> &);

int main() {
    unsigned int currKey;
    unsigned int currValue;
    std::vector<unsigned int> valuesForRadix;
    std::vector<unsigned int> keysForRadix;
    
    std::vector<Tpair> valuesForStd;

    while (std::cin >> currKey >> currValue) {
        valuesForRadix.push_back(currValue);
        keysForRadix.push_back(currKey);
        valuesForStd.push_back(Tpair(currKey, currValue));
    } 
   
    if(keysForRadix.size() > 0) {
        double stdTime = runStdSort(valuesForStd);
        double radixTime = runRadixSort(keysForRadix, valuesForRadix);
        std::cout << valuesForStd.size() << '\t' << radixTime << '\t' << stdTime << std::endl;        
    }
    return 0;
}

bool pairCompair(const Tpair &a, const Tpair &b) {
    return a.first < b.first;
}

double runStdSort(std::vector<Tpair> &data) {
    clock_t start = clock();
    std::sort(data.begin(), data.end(), pairCompair);
    clock_t end = clock();
    double time = (double) (end - start);
    return  time / CLOCKS_PER_SEC;
}
double runRadixSort(std::vector<unsigned int> &keys, std::vector<unsigned int> &values) {
    clock_t start = clock();
    sort(keys, values);
    clock_t end = clock();
    double time = (double) (end - start);
    return  time / CLOCKS_PER_SEC;

}



