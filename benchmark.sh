for test_file in `ls tests/*.t`; do
    echo "Check time test: ${test_file}"
    if ! ./benchmark < $test_file >> time ; then
        echo "ERROR"
        continue
    fi
done 
if ! python3 plot_time.py ; then
echo "ERROR: Failed to python create plot time."
exit 1
fi
