import random
import string
import os

COUNT_TESTS = 20
MAX_DIGIT_VALUE = 2**31
DIR = 'tests/'
if not os.path.exists(DIR):
    os.makedirs(DIR)

if __name__ == "__main__":
    for num in range(1, COUNT_TESTS):
        SIZE_ARRAY = 2**num
        values = list()
        output_filename = DIR + "{:02d}.t".format(num)
        with open(output_filename, 'w') as output:
            for _ in range(0, SIZE_ARRAY+1):
                key = random.randint(0, MAX_DIGIT_VALUE)
                value = random.randint(0, MAX_DIGIT_VALUE)
                values.append((key, value))
                output.write("{}\t{}\n".format(key, value))

        output_filename = DIR + "{:02d}.txt".format(num)
        with open(output_filename, 'w') as output:
            values = sorted(values, key=lambda x: x[0])
            for value in values:
                output.write("{}\t{}\n".format(value[0], value[1]))
