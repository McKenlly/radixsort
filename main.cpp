//Project Radix_sort
#include <iostream>
#include <vector>
#include "sort.h"


int main() {
    unsigned int currKey;
    unsigned int currValue;
    std::vector<unsigned int> values;
    std::vector<unsigned int> keys;

    while (std::cin >> currKey >> currValue) {
        values.push_back(currValue);
        keys.push_back(currKey);
    } 
   
    if(keys.size() > 0) {
        sort(keys, values);
        for (int i = 0 ; i < values.size(); i++) {
            std::cout << keys[i] << '\t' << values[i] << std::endl;
        }
    }
    return 0;
}



