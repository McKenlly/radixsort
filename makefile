
FLAGS=-std=c++11 -pedantic -Wall -Werror -Wno-sign-compare -Wno-long-long -O2
CC=g++

all: sort benchmark main clear runtests 


main: main.cpp
	$(CC) $(FLAGS) -c main.cpp
	$(CC) $(FLAGS) -o run sort.o main.o -lm

benchmark: benchmark.cpp 
	$(CC) $(FLAGS) -c benchmark.cpp
	$(CC) $(FLAGS) -o benchmark sort.o benchmark.o -lm

sort: sort.cpp
	$(CC) $(FLAGS) -c sort.cpp

clear:
	rm -f *.o
	rm -fr *.dSYM
	rm -fr *.gch

runtests:
	rm -rf tests
	mkdir tests
	sh wrapper.sh
	rm tmp
	sh benchmark.sh
	rm benchmark 
	rm time
	