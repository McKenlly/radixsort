import matplotlib.pyplot as plt

count_elem = []
time_std = []
time_radix = []


with open("time", "r") as f:
    for line in f: 
        count_elements, time_std_test, time_radix_test = [float(x) for x in line.split()]
        count_elem.append(count_elements)
        time_std.append(time_std_test)
        time_radix.append(time_radix_test)

plt.ylabel('time, c')
plt.xlabel('amount elements of array')

plt.plot(count_elem, time_std, count_elem, time_radix)
plt.gca().legend(('STD TIME','RADIX TIME'))

plt.savefig('time.png')


        

