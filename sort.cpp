//Project Radix_sort
#include <iostream>
#include <vector>
#include "sort.h"

std::vector<u_short> intToVec(unsigned int& t) {
    std::vector<u_short> result(COUNT_ROWS, 0);
    char i = 0;
    while (t > 0) {
        result[i] = t%1000;
        t /= 1000;
        i++;
    } 
    return result;

}

long long int vecToInt(std::vector<u_short> & t) {
    unsigned int result = 0;
    long long int degree = 1;
    for (int i = 0; i < t.size(); i++) {
        result += degree*t[i];
        degree *= 1000;
    } 
    return result;

}



void countingSort(int shift, int *indexArr,const std::vector<std::vector<u_short> >& keys) {
    int maxDigit = 1000;
    int size = keys.size();
    int *count = new int[maxDigit];
    for (int i = 0; i < maxDigit; i++) {
        count[i] = 0;
    }

    for (int i = 0; i < size; i++) {
        int temp = keys[i][shift];
        count[temp]++;
    }

    for (int i = 1; i < maxDigit; i++) {
        count[i] += count[i-1];
    }

    int *result = new int[size];

    for (int i = (int)size - 1; i >= 0; i--) {
        int tmpKey = keys[indexArr[i]][shift];
        result[count[tmpKey] - 1] = indexArr[i];
        count[tmpKey]--;
    }

    for (int i = 0; i < size; i++) {
       indexArr[i] = result[i];
    }
    //free memory
    delete [] count;
    delete [] result;
}

int* radixSort(const std::vector<std::vector<u_short> >& keys) {
    int *indexArray = new int[keys.size()];
    for (int i = 0; i < keys.size(); i++) indexArray[i] = i;

    for (int shift = 0; shift < COUNT_ROWS; shift++) {
        countingSort(shift, indexArray, keys);
    }
    return indexArray;
}


void sort(std::vector<unsigned int>& array, std::vector<unsigned int>& coArray) {
    std::vector<std::vector<u_short> > keyVec(array.size());
    for (int i = 0; i < array.size(); i++) {
        keyVec[i] = intToVec(array[i]);
    }
    int* arrayIndex = radixSort(keyVec);
    std::vector<unsigned int> sortedValues(keyVec.size());
    std::vector<unsigned int> sortedKeys(keyVec.size());
    for (int  i = 0; i < sortedValues.size(); i++) {
        sortedValues[i] = coArray[arrayIndex[i]];
        sortedKeys[i] = vecToInt(keyVec[arrayIndex[i]]);
    }
    for (int  i = 0; i < sortedValues.size(); i++) {
        coArray[i] = sortedValues[i];
        array[i] = sortedKeys[i];
    }
    delete[] arrayIndex;
}