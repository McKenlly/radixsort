#ifndef SORT_H
#define SORT_H
#include <vector>
#include <iostream>

const char COUNT_ROWS = 4;

std::vector<u_short> intToVec(unsigned int&);
long long int vecToInt(std::vector<u_short> &);

void countingSort(int, int *, const std::vector<std::vector<u_short> >&);
int* radixSort(const std::vector<std::vector<u_short> >&);
void sort(std::vector<unsigned int>&, std::vector<unsigned int>&);

#endif
